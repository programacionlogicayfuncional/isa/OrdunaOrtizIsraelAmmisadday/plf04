(ns plf04.core)

;problema 1
(defn string-E-1
    [x]
    (letfn [(f [x y]
              (if (empty? x)
                (if (and (> y 0) (< y 4))
                  true false)
                (if (= \e (first x))
                  (f (rest x) (inc y))
                  (f (rest x) y))))]
      (f x 0)))


(string-E-1 "Hello")
(string-E-1 "Heelle")
(string-E-1 "Heelele")
(string-E-1 "Hll")
(string-E-1 "e")
(string-E-1 "")

(defn string-E-2
  [x y]
  (letfn [(f [xs acc]
            (if (empty? xs)
              (if (and
                   (> acc 0)
                   (< acc 4))
                true
                false)
              (if (and (= \e (first xs)))
                (f (rest xs) (+ acc 1))
                (f (rest xs) acc))))]
    (f x y)))

(string-E-2 "Hello" 0)
(string-E-2 "Heelle"0)
(string-E-2 "Heelele" 0)
(string-E-2 "Hll" 0)
(string-E-2 "e" 0)
(string-E-2 "" 0)

;problema 2
(defn string-times-1
  [x y]
  (if (== y 0)
    ""
    (str x (string-times-1 x (dec y)))))

(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

(defn string-times-2
  [x y]
  (letfn [(f [x y acc]
            (if (or (zero? y) (empty? x))
              acc
              (str x (f x (dec y) (str acc)))))]
    (f x y "")))

(string-times-2 "Hi" 2)
(string-times-2 "Hi" 3)
(string-times-2 "Hi" 1)
(string-times-2 "Hi" 0)
(string-times-2 "Hi" 5)
(string-times-2 "Oh Boy!" 2)
(string-times-2 "x" 4)
(string-times-2 "" 4)
(string-times-2 "code" 2)
(string-times-2 "code" 3)

;problema 3

(defn front-times-1
  [x y]
  (letfn [(f [x y]
            (if (< (count x) 3)
              (if (zero? y)
                ""
                (str x (f x (dec y))))
              (if (zero? y)
                ""
                (str (subs x 0 3) (f x (dec y))))))]
    (f x y)))

(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

(defn front-times-2
  [x y acc]
  (letfn [(f [x y z]
            (if (< (count x) 3)
              (if (zero? y)
                z
                (f x (dec y) (str x z)))
              (if (zero? y)
                z
                (f x (dec y) (str (subs x 0 3) z)))))]
    (f x y acc)))

(front-times-2 "Chocolate" 2 "")
(front-times-2 "Chocolate" 3 "")
(front-times-2 "Abc" 3 "")
(front-times-2 "Ab" 4 "")
(front-times-2 "A" 4 "")
(front-times-2 "" 4 "")
(front-times-2 "Abc" 0 "")

;problema 4

(defn countx-X-1
  [x]
  (letfn [(f [y z]
             (if (empty? y)
               z
               (if (and (= \x (first y)) (= \x (first (rest y))))
                 (f (rest y)(inc z))
                 (f (rest y) z))))]
    (f x 0)))

(countx-X-1 "abcxx")
(countx-X-1 "xxx")
(countx-X-1 "xxxx")
(countx-X-1 "abc")
(countx-X-1 "Hello there")
(countx-X-1 "Hexxo thxxe")
(countx-X-1 "Kittens")
(countx-X-1 "")
(countx-X-1 "Kittensxxx")

(defn countx-X-2
  [x]
  (letfn [(f [y z acc]
            (if (empty? y)
              acc
              (if (and (= z (first y)) (= z (first (rest y))))
                (f (rest y) z (inc acc))
                (f (rest y) z acc))))]
    (f x \x 0)))

(countx-X-2 "abcxx")
(countx-X-2 "xxx")
(countx-X-2 "xxxx")
(countx-X-2 "abc")
(countx-X-2 "Hello there")
(countx-X-2 "Hexxo thxxe")
(countx-X-2 "")
(countx-X-2 "Kittens")
(countx-X-2 "Kittensxxx")

;problema 5

(defn string-splosion-1
  [x]
  (letfn [(f [y]
            (if (== 0 (count y))
              y
              (apply str (f (subs y 0 (- (count y) 1))) y)))]
    (f x)))

(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "there")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

(defn string-splosion-2
  [x y]
  (letfn [(f [y acc]
            (if (== 0 (count y))
              acc
              (f (subs y 0 (- (count y) 1)) (str y acc))))]
    (f x y)))

(string-splosion-2 "Code" "")
(string-splosion-2 "abc" "")
(string-splosion-2 "ab" "")
(string-splosion-2 "fade" "")
(string-splosion-2 "x" "")
(string-splosion-2 "there" "")
(string-splosion-2 "Kitten" "")
(string-splosion-2 "Bye" "")
(string-splosion-2 "Good" "")
(string-splosion-2 "Bad" "")

; problema 6 

(defn array-123-1
  [x]
  (letfn [(f [ys]
            (if (and (= 1 (first ys))
                     (= 2 (first (rest ys)))
                     (= 3 (first (rest (rest ys)))))
              true (if (empty? ys) false (f (rest ys)))))]

    (f x)))

(array-123-1 [1,1,2,3,1])
(array-123-1 [1,1,2,4,1])
(array-123-1 [1,1,2,3,1])
(array-123-1 [1,1,2,1,2,3])
(array-123-1 [1,1,2,1,2,1])
(array-123-1 [1,2,3,1,2,3])
(array-123-1 [1,2,3])
(array-123-1 [1,1,1])
(array-123-1 [1,2])
(array-123-1 [1])
(array-123-1 [])

(defn array-123-2
  [x]
  (letfn [(f [xs acc]
            (if (empty? xs)
              (and acc false)
              (if (>= (count xs) 3)
                (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                  (and acc true)
                  (f (rest xs) acc))
                (f (empty xs) acc))))]
    (f x "")))

(array-123-2 [1,1,2,3,1]) 
(array-123-2 [1,1,2,4,1])
(array-123-2 [1,1,2,3,1])
(array-123-2 [1,1,2,1,2,3])
(array-123-2 [1,1,2,1,2,1])
(array-123-2 [1,2,3,1,2,3])
(array-123-2 [1,2,3])
(array-123-2 [1,1,1])
(array-123-2 [1,2])
(array-123-2 [1])
(array-123-2 [])

; problema 7

(defn string-X-1
  [x]
  (letfn [(f [y z]
            (if (empty? y)
              ""
              (if (== 0 z)
                (str (first y) (f (rest y) (inc z)))
                (if (and (= \x (first y)) (> (count y) 1))
                  (f (rest y) (inc z))
                  (str (first y) (f (rest y) (inc z)))))))]
    (f x 0)))

(string-X-1 "xxHxix")
(string-X-1 "abxxxcd")
(string-X-1 "xabxxxcdx")
(string-X-1 "xKittenx")
(string-X-1 "Hello")
(string-X-1 "xx")
(string-X-1 "x")
(string-X-1 "")

(defn string-X-2
  [x]
  (letfn [(f [x y acc]
            (if (empty? x)
              acc
              (if (== 0 y)
                (str (first x) (f (rest x) (inc y) (str acc)))
                (if (and (= \x (first x)) (> (count x) 1))
                  (f (rest x) (inc y) (str acc))
                  (str (first x) (f (rest x) (inc y) acc))))))]
    (f x 0 "")))

(string-X-2 "xxHxix")
(string-X-2 "abxxxcd")
(string-X-2 "xabxxxcdx")
(string-X-2 "xKittenx")
(string-X-2 "Hello")
(string-X-2 "xx")
(string-X-2 "x")
(string-X-2 "")

; problema 8
(defn alt-Pairs-1
  [x]
  (letfn [(f [y z]
            (if (== (count y) z)
              ""
              (if (or (== z 0) (== z 1) (== z 4) (== z 5) (== z 8) (== z 9))
                (str (subs y z (+ z 1)) (f x (inc z)))
                (f x (inc z)))))]
    (f x 0)))

(alt-Pairs-1 "kitten")
(alt-Pairs-1 "Chocolate")
(alt-Pairs-1 "CodingHorror")
(alt-Pairs-1 "yak")
(alt-Pairs-1 "ya")
(alt-Pairs-1 "")
(alt-Pairs-1 "ThisThatTheOther")

(defn alt-Pairs-2
  [x]
  (letfn [(f [y z acc]
            (if (== (count y) z)
              acc
              (if (or (== z 0) (== z 1) (== z 4) (== z 5) (== z 8) (== z 9))
                (f y (inc z) (str acc (subs y z (+ z 1))))
                (f y (inc z) acc))))]
    (f x 0 "")))

(alt-Pairs-2 "kitten")
(alt-Pairs-2 "Chocolate")
(alt-Pairs-2 "CodingHorror")
(alt-Pairs-2 "yak")
(alt-Pairs-2 "ya")
(alt-Pairs-2 "")
(alt-Pairs-2 "ThisThatTheOTher")

; problema 9
(defn string-Yak-1
  [x]
  (letfn [(f [y z]
            (if (empty? y)
              ""
              (if (and (= (first y) \y) (= (first (rest y)) \a) (= (first (rest (rest y))) \k) (> (count y) 1))
                (f (rest (rest (rest y))) (inc z))
                (str (first y) (f (rest y) (inc z))))))]
    (f x 0)))
 
(string-Yak-1 "yakpak")
(string-Yak-1 "pakyak")
(string-Yak-1 "yak123ya")
(string-Yak-1 "yak")
(string-Yak-1 "yakxxxyak")
(string-Yak-1 "HiyakHi")
(string-Yak-1 "xxxyakyyyakzzz")

(defn string-Yak-2
  [x]
  (letfn [(f [y z acc]
            (if (empty? y)
              acc
              (if (and (= (first y) \y) (= (first (rest y)) \a) (= (first (rest (rest y))) \k) (> (count y) 1))
                (f (rest (rest (rest y))) (inc z) acc)
                (f (rest y) (inc z) (str acc (first y))))))]
    (f x 0 "")))

(string-Yak-2 "yakpak")
(string-Yak-2 "pakyak")
(string-Yak-2 "yak123ya")
(string-Yak-2 "yak")
(string-Yak-2 "yakxxxyak")
(string-Yak-2 "HiyakHi")
(string-Yak-2 "xxxyakyyyakzzz")

;problema 10

(defn has-271-1
  [x]
  (letfn [(f [ys]
            (if (or (<= (count ys) 2) (empty? ys))
              false
              (if (and
                   (== (first (rest ys)) (+ (first ys) 5))
                   (<= (if (pos? (- (first (rest (rest ys))) (dec (first ys))))
                         (- (first (rest (rest ys))) (dec (first ys)))
                         (* -1 (- (first (rest (rest ys))) (dec (first ys))))) 2))
                true
                (f (rest ys)))))]
    (f x)))

(has-271-1 [1 2 7 1])
(has-271-1 [1 2 8 1])
(has-271-1 [2 7 1])
(has-271-1 [3 8 2])
(has-271-1 [2 7 3])
(has-271-1 [2 7 4])
(has-271-1 [2 7 -1])
(has-271-1 [2 7 -2])
(has-271-1 [4 5 3 8 0])
(has-271-1 [2 7 5 10 4])
(has-271-1 [2 7 -2 4 9 3])
(has-271-1 [2 7 5 10 1])
(has-271-1 [2 7 -2 4 10 2])
(has-271-1 [1 1 4 9 0])
(has-271-1 [1 1 4 9 4 9 2])

(defn has-271-2
  [x acc]
  (letfn [(f [ys y]
            (if (empty? ys)
              false
              (if (>= (count ys) 3)
                (if (and (== (- (first (rest ys)) (first ys)) 5)
                         (>= 2 (if (neg? (- (first (rest (rest ys))) (- (first ys) 1)))
                                 (* -1 (- (first (rest (rest ys))) (- (first ys) 1)))
                                 (* 1 (- (first (rest (rest ys))) (- (first ys) 1))))))
                  true
                  (f (rest ys) y))
                (f (empty ys) y))))]
    (f x acc)))

(has-271-2 [1 2 7 1] "")
(has-271-2 [1 2 8 1] "")
(has-271-2 [2 7 1] "")
(has-271-2 [3 8 2] "")
(has-271-2 [2 7 3] "")
(has-271-2 [2 7 4] "")
(has-271-2 [2 7 -1] "")
(has-271-2 [2 7 -2] "")
(has-271-2 [4 5 3 8 0] "")
(has-271-2 [2 7 5 10 4] "")
(has-271-2 [2 7 -2 4 9 3] "")
(has-271-2 [2 7 5 10 1] "")
(has-271-2 [2 7 -2 4 10 2] "")
(has-271-2 [1 1 4 9 0] "")
(has-271-2 [1 1 4 9 4 9 2] "")